package com.example.gabrielnorena.firebase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gabrielnorena.firebase.Objetos.FirebaseReferences;
import com.example.gabrielnorena.firebase.Objetos.Servicio;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SolicitarServicio extends AppCompatActivity implements View.OnClickListener{

    EditText edtDireccionSalida, edtDireccionLlegada, edtFechaRecogida, edtHoraRecogida, edtDescripcion;
    Button btnRegistrarServicio;
    DatabaseReference referenciaServicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitar_servicio);



        edtDireccionSalida = (EditText) findViewById(R.id.edtDireccionSalida);
        edtDireccionLlegada = (EditText) findViewById(R.id.edtDireccionLlegada);
        edtFechaRecogida = (EditText) findViewById(R.id.edtFechaRecogida);
        edtHoraRecogida = (EditText) findViewById(R.id.edtHoraRecogida);
        edtDescripcion = (EditText) findViewById(R.id.edtDescripcion);

        btnRegistrarServicio = (Button) findViewById(R.id.boton_registrarServicio);

        btnRegistrarServicio.setOnClickListener(this);



        FirebaseDatabase baseDatos = FirebaseDatabase.getInstance();

        referenciaServicio = baseDatos.getReference(FirebaseReferences.SERVICIO_REFERENCE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.boton_registrarServicio:
                Servicio servicio = new Servicio();
                servicio.setUsuario(1);
                servicio.setTransportador(0);
                servicio.setDireccionSalida(edtDireccionSalida.getText().toString());
                servicio.setDireccionLlegada(edtDireccionLlegada.getText().toString());
                servicio.setFechaRecogida(edtFechaRecogida.getText().toString());
                servicio.setHoraRecogida(edtHoraRecogida.getText().toString());
                servicio.setDescripcion(edtDescripcion.getText().toString());
                servicio.setEstado("Solicitado");

                referenciaServicio.child(FirebaseReferences.SERVICIO_REFERENCE).push().setValue(servicio);


                Toast toast1 =
                        Toast.makeText(getApplicationContext(),
                                "Servicio Solicitado", Toast.LENGTH_LONG);

                toast1.show();

                Intent inicio = new Intent(SolicitarServicio.this,InicioUsuario.class);
                startActivity(inicio);


                break;
        }
    }
}
