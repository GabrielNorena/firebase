package com.example.gabrielnorena.firebase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class InicioUsuario extends AppCompatActivity implements View.OnClickListener  {

    Button btnSolicitarServicio, btnMisServicios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_usuario);

        btnSolicitarServicio = (Button) findViewById(R.id.boton_SolicitarServicio);
        btnMisServicios = (Button) findViewById(R.id.boton_MisServicios);

        btnSolicitarServicio.setOnClickListener(this);
        btnMisServicios.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.boton_SolicitarServicio:
                Intent solicitar = new Intent(InicioUsuario.this, SolicitarServicio.class);
                startActivity(solicitar);
                break;

            case R.id.boton_MisServicios:
                Intent misServicios = new Intent(InicioUsuario.this, MisServicios.class);
                startActivity(misServicios);
                break;

        }

    }
}
